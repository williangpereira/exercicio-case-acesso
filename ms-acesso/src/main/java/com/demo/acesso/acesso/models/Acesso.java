package com.demo.acesso.acesso.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Acesso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private int id_acesso;

    @JsonProperty(value = "porta_id")
    private int porta_id;

    @JsonProperty(value = "cliente_id")
    private int cliente_id;

    public Acesso() {}

    public Acesso(int porta_id, int cliente_id) {
        this.porta_id = porta_id;
        this.cliente_id = cliente_id;
    }

    public int getPorta_id() {
        return porta_id;
    }

    public void setPorta_id(int porta_id) {
        this.porta_id = porta_id;
    }

    public int getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(int cliente_id) {
        this.cliente_id = cliente_id;
    }

    public int getId_acesso() {
        return id_acesso;
    }

    public void setId_acesso(int id_acesso) {
        this.id_acesso = id_acesso;
    }
}
