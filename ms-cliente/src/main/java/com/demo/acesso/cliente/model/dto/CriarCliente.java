package com.demo.acesso.cliente.model.dto;

public class CriarCliente {

    private String nome;

    public CriarCliente() {}

    public CriarCliente(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
