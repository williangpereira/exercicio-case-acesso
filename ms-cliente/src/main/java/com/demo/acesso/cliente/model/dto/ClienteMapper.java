package com.demo.acesso.cliente.model.dto;

import com.demo.acesso.cliente.model.Cliente;
import org.springframework.stereotype.Component;

@Component
public class ClienteMapper {

    public Cliente paraCliente(CriarCliente criarCliente){
       Cliente cliente = new Cliente();
       cliente.setNome(criarCliente.getNome());
       return cliente;
    }

}
