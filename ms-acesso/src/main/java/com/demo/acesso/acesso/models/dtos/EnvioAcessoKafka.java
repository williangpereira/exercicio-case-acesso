package com.demo.acesso.acesso.models.dtos;

import java.time.LocalDateTime;

public class EnvioAcessoKafka {
    private int id_acesso;
    private int id_porta;
    private int id_cliente;
    private LocalDateTime dataHoraAcesso;

    public EnvioAcessoKafka() {
    }

    public EnvioAcessoKafka(int idAcesso, int idPorta, int idCliente, LocalDateTime dataHoraAcesso) {
        this.id_acesso = idAcesso;
        this.id_porta = idPorta;
        this.id_cliente = idCliente;
        this.dataHoraAcesso = dataHoraAcesso;
    }

    public int getIdAcesso() {
        return id_acesso;
    }

    public void setIdAcesso(int idAcesso) {
        this.id_acesso = idAcesso;
    }

    public int getIdPorta() {
        return id_porta;
    }

    public void setIdPorta(int idPorta) {
        this.id_porta = idPorta;
    }

    public int getIdCliente() {
        return id_cliente;
    }

    public void setIdCliente(int idCliente) {
        this.id_cliente = idCliente;
    }

    public LocalDateTime getDataHoraAcesso() {
        return dataHoraAcesso;
    }

    public void setDataHoraAcesso(LocalDateTime dataHoraAcesso) {
        this.dataHoraAcesso = dataHoraAcesso;
    }

    @Override
    public String toString() {
        return "EnvioAcessoKafka{" +
                "idAcesso=" + id_acesso +
                ", idPorta=" + id_porta +
                ", idCliente=" + id_cliente +
                ", dataHoraAcesso=" + dataHoraAcesso +
                '}';
    }


}
