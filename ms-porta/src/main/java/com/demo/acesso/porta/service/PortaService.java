package com.demo.acesso.porta.service;

import com.demo.acesso.porta.exceptions.PortaNaoEncontradaException;
import com.demo.acesso.porta.model.Porta;
import com.demo.acesso.porta.repository.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta criar(Porta porta){
        return portaRepository.save(porta);
    }

    public Porta buscarPorID(int id){

        Optional<Porta> buscarPorID = portaRepository.findById(id);

        if(!buscarPorID.isPresent()){
            throw new PortaNaoEncontradaException();
        }

        return buscarPorID.get();
    }


}
