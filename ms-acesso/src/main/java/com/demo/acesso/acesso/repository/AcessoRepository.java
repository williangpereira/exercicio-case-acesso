package com.demo.acesso.acesso.repository;

import com.demo.acesso.acesso.models.Acesso;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AcessoRepository extends CrudRepository<Acesso, Integer> {
    Optional<Acesso> findByIdClienteAndIdPorta(int id_cliente, int id_porta);
}
