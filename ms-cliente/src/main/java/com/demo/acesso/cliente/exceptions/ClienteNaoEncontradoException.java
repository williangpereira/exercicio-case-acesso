package com.demo.acesso.cliente.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Cliente não encontrado!")
public class ClienteNaoEncontradoException extends RuntimeException {
}
