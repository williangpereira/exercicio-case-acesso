package com.demo.acesso.cliente.repository;

import com.demo.acesso.cliente.model.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
}
