package com.demo.acesso.porta.repository;

import com.demo.acesso.porta.model.Porta;
import org.springframework.data.repository.CrudRepository;

public interface PortaRepository extends CrudRepository<Porta, Integer> {
}
