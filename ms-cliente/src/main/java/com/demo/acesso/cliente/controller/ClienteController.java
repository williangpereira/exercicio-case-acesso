package com.demo.acesso.cliente.controller;

import com.demo.acesso.cliente.model.Cliente;
import com.demo.acesso.cliente.model.dto.ClienteMapper;
import com.demo.acesso.cliente.model.dto.CriarCliente;
import com.demo.acesso.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cliente")
public class    ClienteController {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteMapper clienteMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente criarCliente(@RequestBody CriarCliente criarCliente){
        Cliente cliente = clienteMapper.paraCliente(criarCliente);

        clienteService.criar(cliente);

        return cliente;
    }

    @GetMapping("/{id_cliente}")
    public Cliente clientePorID(@PathVariable int id_cliente){
        return clienteService.buscarPorID(id_cliente);
    }

}
