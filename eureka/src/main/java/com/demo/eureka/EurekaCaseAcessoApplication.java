package com.demo.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EurekaCaseAcessoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaCaseAcessoApplication.class, args);
	}

}
