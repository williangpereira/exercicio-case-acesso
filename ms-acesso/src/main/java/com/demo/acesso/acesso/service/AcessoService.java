package com.demo.acesso.acesso.service;

import com.demo.acesso.acesso.clients.ClienteClient;
import com.demo.acesso.acesso.clients.PortaClient;
import com.demo.acesso.acesso.exceptions.AcessoNaoEncontradoException;
import com.demo.acesso.acesso.models.Acesso;
import com.demo.acesso.acesso.repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private PortaClient portaClient;

    public Acesso criar(Acesso acesso){
        int idCliente = clienteClient.clientePorID(acesso.getCliente_id());
        int idPorta = portaClient.buscarPorID(acesso.getPorta_id());

        acesso.setCliente_id(idCliente);
        acesso.setPorta_id(idPorta);

        return acessoRepository.save(acesso);
    }

    public Acesso consultarAcesso(Acesso acesso) {
        Optional<Acesso> acessoOptional = acessoRepository.findByIdClienteAndIdPorta(acesso.getCliente_id(),
                acesso.getPorta_id());
        if (!acessoOptional.isPresent()) {
            throw new AcessoNaoEncontradoException();
        }
        return acessoOptional.get();
    }


    public void deletarAcesso(Acesso acesso){
        acesso = consultarAcesso(acesso);
        acessoRepository.deleteById(acesso.getId_acesso());
    }

}
