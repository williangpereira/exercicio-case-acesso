package com.demo.acesso.acesso.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="porta")
public interface PortaClient {

    @GetMapping("porta/{id_porta}")
    int buscarPorID(@PathVariable int id_porta);
}
