package com.demo.acesso.porta.controller;

import com.demo.acesso.porta.model.Porta;
import com.demo.acesso.porta.service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    @PostMapping
    public Porta criar(@RequestBody Porta porta){
        return portaService.criar(porta);
    }

    @GetMapping("/{id_porta}")
    public Porta buscarPorID(@PathVariable int id_porta){
        return portaService.buscarPorID(id_porta);
    }


}
