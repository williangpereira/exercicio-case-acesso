package com.demo.acesso.cliente.service;

import com.demo.acesso.cliente.exceptions.ClienteNaoEncontradoException;
import com.demo.acesso.cliente.model.Cliente;
import com.demo.acesso.cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criar(Cliente cliente){
        Cliente criarCliente = clienteRepository.save(cliente);
        return criarCliente;
    }

    public Cliente buscarPorID(int id){
        Optional<Cliente> clientePorID = clienteRepository.findById(id);

        if(!clientePorID.isPresent()){
            throw new ClienteNaoEncontradoException();
        }
        return clientePorID.get();
    }


}
