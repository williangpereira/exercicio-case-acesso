package com.demo.acesso.acesso.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="cliente")
public interface ClienteClient {

    @GetMapping("cliente/{id_cliente}")
    int clientePorID(@PathVariable int id_cliente);
}
