package com.demo.acesso.acesso.controller;

import com.demo.acesso.acesso.models.Acesso;
import com.demo.acesso.acesso.service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Acesso criar(@RequestBody Acesso acesso){

        return acessoService.criar(acesso);
    }

    @GetMapping("/{cliente_id}/{porta_id}")
    public Acesso consultarAcesso(@PathVariable int cliente_id, @PathVariable int porta_id) {
        Acesso acesso = new Acesso();
        acesso.setCliente_id(cliente_id);
        acesso.setPorta_id(porta_id);
        acessoService.consultarAcesso(acesso);
        return acesso;
    }


    @DeleteMapping("/{cliente_id}/{porta_id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarAcesso(@PathVariable int cliente_id, @PathVariable int porta_id) {
        Acesso acesso = new Acesso();
        acesso.setCliente_id(cliente_id);
        acesso.setPorta_id(porta_id);
        acessoService.deletarAcesso(acesso);
    }

}
