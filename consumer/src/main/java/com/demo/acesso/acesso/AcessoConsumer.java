package com.demo.acesso.acesso;

import com.demo.acesso.acesso.models.dto.EnvioAcessoKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;

@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec3-willian-garcia-1", groupId = "Willian")
    public void receber(@Payload EnvioAcessoKafka envioAcessoKafka) {
        generateCsvFile("/home/a2/workspace/Especialização/exercicio-case-acesso/logKafka.csv", envioAcessoKafka);
    }

    private static void generateCsvFile(String sFileName, EnvioAcessoKafka envioAcessoKafka) {
        try {
            FileWriter writer = new FileWriter(sFileName);

            writer.append(envioAcessoKafka.toString());

            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
