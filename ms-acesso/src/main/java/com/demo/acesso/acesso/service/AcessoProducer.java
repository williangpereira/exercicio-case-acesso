package com.demo.acesso.acesso.service;

import com.demo.acesso.acesso.models.Acesso;
import com.demo.acesso.acesso.models.dtos.EnvioAcessoKafka;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class AcessoProducer {

    @Autowired
    private KafkaTemplate<String, EnvioAcessoKafka> acessoKafka;

    public void EnviarAoKafka(Acesso acesso) {
        EnvioAcessoKafka envioAcessoKafka = new EnvioAcessoKafka(acesso.getId_acesso(), acesso.getPorta_id(),
                acesso.getCliente_id(), LocalDateTime.now());
        acessoKafka.send("spec3-willian-garcia-1", envioAcessoKafka);
    }

}
