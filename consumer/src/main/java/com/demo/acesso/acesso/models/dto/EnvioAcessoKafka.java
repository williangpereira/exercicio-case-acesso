package com.demo.acesso.acesso.models.dto;

import java.time.LocalDateTime;

public class EnvioAcessoKafka {
    private int id_acesso;
    private int id_porta;
    private int id_cliente;
    private LocalDateTime dataHoraAcesso;

    public EnvioAcessoKafka() {}

    public EnvioAcessoKafka(int id_acesso, int id_porta, int id_cliente, LocalDateTime dataHoraAcesso) {
        this.id_acesso = id_acesso;
        this.id_porta = id_porta;
        this.id_cliente = id_cliente;
        this.dataHoraAcesso = dataHoraAcesso;
    }

    public int getId_acesso() {
        return id_acesso;
    }

    public void setId_acesso(int id_acesso) {
        this.id_acesso = id_acesso;
    }

    public int getId_porta() {
        return id_porta;
    }

    public void setId_porta(int id_porta) {
        this.id_porta = id_porta;
    }

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public LocalDateTime getDataHoraAcesso() {
        return dataHoraAcesso;
    }

    public void setDataHoraAcesso(LocalDateTime dataHoraAcesso) {
        this.dataHoraAcesso = dataHoraAcesso;
    }
}
